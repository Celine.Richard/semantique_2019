import LogicKit
import LogicKitBuiltins

let x: Term = .var("x")
let y: Term = .var("y")
let z: Term = .var("z")

let first:  Term  = .var("first")
let last :  Term  = .var("last")
let head :  Term  = .var("head")
let tail :  Term  = .var("tail")

let h1: Term = .var("h1")
let t1: Term = .var("t1")
let h2: Term = .var("h2")
let t2: Term = .var("t2")

let res: Term = .var("res")

var kb: KnowledgeBase = [
  
  // Complete the knowledge base !
  // ...
  
]

kb = kb + KnowledgeBase(knowledge: (Nat.axioms + List.axioms))

// Write test below to test your rules !

//let req = kb.ask(.fact("op", x, ...))
//for binding in req{
//  print(" Ma fact: ", binding["res"]!)
//}
